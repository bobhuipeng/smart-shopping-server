var fs = require('fs');
var express = require('express');
var https = require('https');
var key = fs.readFileSync('./hacksparrow-key.pem');
var cert = fs.readFileSync('./hacksparrow-cert.pem')

var https_options = {
    key: key,
    cert: cert
};
var PORT = 9090;
var HOST = '192.168.1.101';
app = express();

// app.configure(function(){
//     app.use(app.router);
// });

server = https.createServer(https_options, app).listen(PORT, HOST,() => {
    console.info('HTTPS Server listening on %s:%s', HOST, PORT);
  });



app.get('/', function (req, res) {
  res.send('Hello World!');
});



app.get('/todoList/stormswan',(req, res) =>{
     res.json(mokeTodoList);
})


const mokeTodoList ={
    "list01" : {
      "apple" : {
        "weight" : 500
      },
      "banana" : {
        "weight" : 300
      }
    },
    "list02" : {
      "Milk" : {
        "bottle" : 2
      },
      "apple" : {
        "weight" : 500
      },
      "battery" : {
        "aaa" : 10
      },
      "orange" : {
        "weight" : 300
      }
    },
    "list03" : {
      "Milk" : {
        "bottle" : 2
      },
      "battery" : {
        "aaa" : 10
      },
      "fish" : {
        "weight" : 500
      },
      "orange" : {
        "weight" : 300
      }
    }
  }
  