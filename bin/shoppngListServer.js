var express = require('express');
var app = express();

var bodyParser = require('body-parser');
// var multer = require('multer'); // v1.0.5
// var upload = multer(); // for parsing multipart/form-data


const MongoClient = require('mongodb').MongoClient;
const f = require('util').format;
const assert = require('assert');

const TextRecongnise = require('../server/recongise/TextRecongnise')

const user = encodeURIComponent('smartshopping');
const password = encodeURIComponent('myshoppinglist');
const authMechanism = 'DEFAULT';

let mdb;

const mongodbUri=  'mongodb://%s:%s@192.168.99.100:27017/?authMechanism=%s'
//const url = f('mongodb://%s:%s@192.168.99.100:27017/?authMechanism=%s',
 // user, password, authMechanism);
  const url = 'mongodb://smartshopping:myshoppinglist@192.168.99.100:27017/?authMechanism=SCRAM-SHA-1&authSource=smartshopping'
const dbName = 'smartshopping'


MongoClient.connect(url, (err,client)=>{
    console.log('start connect to Mongo DB ');
    assert.equal(null,err)
    console.log('connect to Mongo DB ');

    const db = client.db(dbName);
    
    mdb = db

})



var PORT = 9090;
var HOST = '192.168.1.101';

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded




app.get('/', function (req, res) {
  res.send('Hello World!');
});



app.get('/todoList/stormswan',(req, res) =>{
     res.json(mokeTodoList);
})

app.get('/shopping/list/:username',(req, res) =>{
  
  const username = req.params.username;
  console.log('search shopping list history: ',username);

  let shoppingHistory = [];
  const col= mdb.collection(username).find({})
     col.each((err, historylist) => {
       assert.equal(null, err);
       if (!historylist) { // no more historylists
         res.send({ shoppingHistory });
         return;
       }
       shoppingHistory.push(historylist);
     });
})

/**
 * insert new shopping list, if the name exist, it will return an
 * error. and did not insert the new record.
 */
app.post('/shopping/list/:username',(req, res, next) =>{
  console.log('========/shopping-list========')
  const username = req.params.username;
  console.log('search shopping list history: ',username);
  console.log(req.body);
  console.log('================')
  
  mdb.collection(username).count({name:req.body.name},(error, recordCount)=>{
    console.log('======== shopping list name count- name: ',req.body.name,recordCount)
    if(error !== null){
      res.send({error: 'system error name',success:false, errorInfo:error});
      return;
    }

    if(recordCount >0 ){
      res.send({error: 'dupicate name',success:false});
      return;
    }
  
    mdb.collection(username).insertOne(req.body)
  
    res.send({success:true});
  })
  

})

/**
 * if the shopping list exist, it will update shopping 
 * list content. 
 * 
 * if the shopping list is not exist, it will create a
 * new shopping list
 */
app.put('/shopping/list/:username',(req, res, next) =>{
  console.log('========/shopping-list-put========')
  const username = req.params.username;
  console.log('search shopping list history: '+username);
  console.log(req.body);
  console.log('================')
 
  mdb.collection(username).findOneAndReplace({name:req.body.name},req.body,{ upsert: true},(error, oldList)=>{
    console.log('======== shopping list name update/insert- name: ',req.body.name,oldList)
    if(error !== null){
      res.send({error: 'system error name',success:false, errorInfo:error});
      return;
    }
  
    res.send({success:true,newList:oldList});
  })
})


app.post('/shopping/habit/:username',(req, res, next) =>{ 
  console.log('=======user/shopping/habit=========')
  console.log(req.body);
  console.log('================')

  const username = req.params.username;
  console.log('search shopping list history: ',username);

  let shoppingHistory = [];
  const col= mdb.collection(username).find({})
     col.each((error, historylist) => {
      if(error !== null){
        console.error(error);
        res.send({error: 'system error name',success:false, errorInfo:error});
        return;
      }

       if (!historylist) { // no more historylists
         res.send({ shoppingHistory });
         return;
       }

       historylist.items.forEach((item) =>{addNewItemToHabit(item,shoppingHistory)})
     });
})


app.post('/shopping/trans/',(req, res, next) =>{ 
  // console.log('=======user/shopping/habit=========')
  // console.log(req.body);
  // console.log('================')

  // const username = req.params.username;
  // console.log('search shopping list history: ',username);

  let shoppingHistory = [];
  let filePath ='';
  let tr = new TextRecongnise()
  tr.receptionToShoppingList(req.body.filePath);
})


app.listen(PORT, HOST ,function () {
  console.log('Example app listening on port 9090!');
})


const addNewItemToHabit = (newItem,itemHabitList) =>{
  let exist = itemHabitList.filter((item) =>item.name === newItem.name)
  //not exist, add to list
  if(exist==undefined || exist.length === 0){
    itemHabitList.push(newItem)
  }
 
}
;

mokeShoppingHabit= [
    {id:1,name:"cake" ,count: 2, unit: "box", isDone: false},
    {id:2,name:"battery" ,count: 10, unit: "aaa", isDone: false},
    {id:3,name:"fish" ,count: 2, unit: "kg", isDone: false},
    {id:4,name:"banana" ,count: 2, unit: "kg", isDone: false},
    {id:5,name:"apple" ,count: 2, unit: "kg", isDone: false},
    {id:6,name:"oil" ,count: 2, unit: "bottle", isDone: false}
]  

var mokeTodoList =[
  {name:"list01" ,items: [
     {id:1,name:"grape" ,count: 500, unit: "g", isDone: false},
      {id:2,name:"apple" ,count: 1, unit: "kg", isDone: false},
   ]},
   {name:"list02" ,items:  [
     {id:1,name:"cake" ,count: 2, unit: "box", isDone: false},
     {id:2,name:"battery" ,count: 10, unit: "aaa", isDone: false},
     {id:3,name:"fish" ,count: 2, unit: "kg", isDone: false},
   ]},
      {name:"list03" ,items: [
     {id:1,name:"milk" ,count: 2, unit: "bottle", isDone: false},
     {id:2,name:"rice" ,count: 3, unit: "bag", isDone: false},
     {id:3,name:"orange" ,count: 1, unit: "kg", isDone: false},
   ]}
]
