const MongoClient = require('mongodb').MongoClient;
const f = require('util').format;
const assert = require('assert');

const user = encodeURIComponent('smartshopping');
const password = encodeURIComponent('myshoppinglist');
const authMechanism = 'DEFAULT';

let mdb;

const mongodbUri=  'mongodb://%s:%s@192.168.99.100:27017/?authMechanism=%s'
//const url = f('mongodb://%s:%s@192.168.99.100:27017/?authMechanism=%s',
 // user, password, authMechanism);
  const url = 'mongodb://smartshopping:myshoppinglist@192.168.99.100:27017/?authMechanism=SCRAM-SHA-1&authSource=smartshopping'
const dbName = 'smartshopping'


MongoClient.connect(url, (err,client)=>{
    console.log('start connect to Mongo DB ');
    assert.equal(null,err)
    console.log('connect to Mongo DB ');

    const db = client.db(dbName);
    
    mdb = db

    insertDocuments(db, function() {
        client.close();
      });
})


const insertDocuments = function( db,callback) {
    // Get the documents collection
    const collection = db.collection('stormswan');
    // Insert some documents
    collection.insertMany(mokeTodoList, function(err, result) {
      assert.equal(err, null);
      assert.equal(3, result.result.n);
      assert.equal(3, result.ops.length);
      console.log("Inserted 3 documents into the collection");
      callback(result);
    });
  }

  //insertDocuments(mdb,()=>{})

  var mokeTodoList =[
    {name:"list01" ,items: [
       {id:1,name:"grape" ,count: 500, unit: "g", isDone: false},
        {id:2,name:"apple" ,count: 1, unit: "kg", isDone: false},
     ]},
     {name:"list02" ,items:  [
       {id:1,name:"cake" ,count: 2, unit: "box", isDone: false},
       {id:2,name:"battery" ,count: 10, unit: "aaa", isDone: false},
       {id:3,name:"fish" ,count: 2, unit: "kg", isDone: false},
     ]},
        {name:"list03" ,items: [
       {id:1,name:"milk" ,count: 2, unit: "bottle", isDone: false},
       {id:2,name:"rice" ,count: 3, unit: "bag", isDone: false},
       {id:3,name:"orange" ,count: 1, unit: "kg", isDone: false},
     ]}
  ]
  