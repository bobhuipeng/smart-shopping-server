'use strict';

const {
    RTMClient
} = require('@slack/client');

let rtm = null;

let nlp = null;

// const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;//Note exist

// An access token (from your Slack app or custom integration - usually xoxb)
//const token = 'xoxb-350412265990-381026343095-8CkXxkLRku3yYwxiDiVKJICf';//process.env.SLACK_TOKEN;

function handleOnAuthenticated(rtmStartData) {
    console.log(`Logged in as ${rtmStartData.self.name} of team
                ${rtmStartData.team.name}, but not yet connected to a channel!
    `);

}

function addauthenticatedHandler(rtm, handler) {
    // rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED,handler);
    rtm.on('message', (event) => {
        // Structure of `event`: <https://api.slack.com/events/message>
        // console.log(`Message from ${event.user}: ${event.text}`);
        console.log('==============================');

        console.log(`Message from ${event.team.name}: ${event.self.name}`);
    })
}

function handleOnMessage(message) {
    console.log('------------------------------');

    console.log(message);
    nlp.ask(message.text, (err, res) => {
        if (err) {
            console.log(err);
            return;

        }

        console.log(res.intent);
        console.log(res.intent[0]);
        console.log(res.location);
        
        if(!res.intent){
            rtm.sendMessage('Sorry I am did not understand there', 'CB5925WG0')
            .then((res) => {
                // `res` contains information about the posted message
                console.log('Message sent: ', res.ts);
            })
            .catch(console.error);
        }else if(res.intent[0].value == 'time' && res.location){
            rtm.sendMessage(`I don't yet know the time in ${res.location[0].value}`, 'CB5925WG0')
            .then((res) => {
                // `res` contains information about the posted message
                console.log('Message sent: ', res.ts);
            })
            .catch(console.error);
        }else {

            rtm.sendMessage('Sorry I am did not understand what you are talking', 'CB5925WG0')
            .then((res) => {
                // `res` contains information about the posted message
                console.log('Message sent: ', res.ts);
            })
            .catch(console.error);
        }

    })
}

module.exports.init = (token, logLevel, nlpClient) => {
    rtm = new RTMClient(token); // new RTMClient(token,{logLevel: logLevel});
    nlp = nlpClient;
    // addauthenticatedHandler(rtm,handleOnAuthenticated);
    rtm.on('message', handleOnMessage)
    return rtm;
}
// The client is initialized and then started to get an active connection to the platform

// rtm.start();



module.exports.addauthenticatedHandler = addauthenticatedHandler;