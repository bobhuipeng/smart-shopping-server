'use strict';
// var images = require('images')
var Tesseract = require('tesseract.js');
// var request = require('request');
var fs = require('fs');


const path = require("path");
const TransReceToRecord = require('./TransReceToRecord')


const options = {
    langPath: path.join(__dirname, "langs") // Or wherever your downloaded langs are stored
};


module.exports = class TextRecongnise {
    constructor() {
        // tesseractPromise = tesseract.create({ langPath: "eng.traineddata" });
    }

    receptionToShoppingList(filePath) {
        console.log('text recongize');

        let myImage = path.resolve(__dirname, 'data/cosmic.png') //'E:/React/exercise/smart-shopping/app/api/demo.gif';
        let myImage2 = path.resolve(__dirname, filePath)
        Tesseract
            .recognize(myImage2, 'eng')
            // .progress(message => console.log('message: ',message))
            .then(data => {
                // console.log('then\n', data.text)
                let trr =new TransReceToRecord()
                trr.trantToShoppingList(data.text)
            })
            .catch(err => {
                console.log('catch\n', err);
            })
            .finally(e => {
                console.log('finally\n');
                // process.exit();
            });
    };
}
