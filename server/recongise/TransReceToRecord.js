/**
 * this class is translate receptions to Shopping list record
 */

//TODO: get from DB future
const storeNameList = [{
    name: 'ALDI',
    recepName: ['ALDI STORES']
  },
  {
    name: 'WoolWorth',
    recepName: ['WOOLWORTH']
  }
]


//TODO: read from DB
const maxLineForStoreName = 5
const maxLineForStoreItem = 100

module.exports = class TransReceToRecord {
  constructor() {
    // tesseractPromise = tesseract.create({ langPath: "eng.traineddata" });
  }

  trantToShoppingList(receptionText) {
    console.log('+++++++++++++++++++++++++++++++');
    let recepTextList = receptionText.split("\n")
    let shoppingList = {
      name: '',
      items: []
    }

    shoppingList.name = this.getStoreName(recepTextList)
    console.log('Store Name: ',shoppingList.name);
    shoppingList.name= 'ALDI'
    this.getItemList(recepTextList,shoppingList.name)
  }

  getStoreName(recepTextList) {
    let storeName =''
    let maxLineNo =maxLineForStoreName
    recepTextList.forEach(
      (line, index) => {
        storeNameList.forEach((store) => {
          if (line.includes(store.name)) {
            maxLineNo = index
            storeName=  store.name
            return storeName
          }
        })

        if (index > maxLineNo) {
          return ''
        }
      }
    )
    return storeName
  }

  getItemList(text, storeName) {
    let itemList = []
    if(storeName === 'ALDI'){
      itemList= this.getALDIStoreItemList(text)
    }
    
  }

  getALDIStoreItemList(text) {
    let subtotal = 0;
    let itemList = []
    let ABNLineNo = 0;
    let dollarLineNo = 0;
    let itemListMaxNo = maxLineForStoreItem;
    console.log('------- item list process');
    
    text.forEach(
      (line, index) => {
        //Do not read the line after max line number
        if (index < itemListMaxNo) {
          if (line.trim().startsWith('ABN:')) {
            ABNLineNo = index;
            console.log('abn line no: ',ABNLineNo);
            
          } else
          if (line.toLowerCase().includes('subtotal') && isNaN(line.replace('subtotal', ''))) {
            subtotal = line.toLowerCase().replace('subtotal', '')
            itemListMaxNo = index
          } else {
            let itemCoust = 0
            if ((index > ABNLineNo + 1)) {
              let item = {
                name: '',
                cost: 0,
                quantity: '',
              }
              console.log('line',line);
              console.log('size: ', line.split(' ').length);
              line.split(' ').forEach((word, lineIndex, array) => {
                console.log('word:',lineIndex,' - ',word);
                
                //get item name
                if (lineIndex > 1 && lineIndex < (array.length - 2)) {
                  if (lineIndex === (array.length - 3) && this.isQuantity(word)) {
                    item.quantity = word
                  } else {
                    item.name = item.name + word
                  }
                }
                if (lineIndex === (array.length - 2 ) && !isNaN(word)) {
                  item.cost = word
                }
              })
              if(item.name !== '' && item.cost !== 0){
                itemList.push(item)
              }
              
            }
          }

        }
      }
    )

    console.log('Item list: ',itemList);
    console.log('Total cost: ',subtotal);
    
    
  }

  isQuantity(word){
    //replace the potencial identity 1 to l and 0 to o. 
    let lowCaseWord =word.toLowerCase().replace('l','1').replace('o','0')
    
    let result =((lowCaseWord.trim().endsWith('g') && !isNaN(lowCaseWord.replace('g',''))) 
    || (lowCaseWord.trim().endsWith('kg')&&!isNaN(lowCaseWord.replace('kg',''))))
    
   return result
  }
}