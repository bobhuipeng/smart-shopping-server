const mokeTodoList ={
    "list01" : {
      "apple" : {
        "weight" : 500
      },
      "banana" : {
        "weight" : 300
      }
    },
    "list02" : {
      "Milk" : {
        "bottle" : 2
      },
      "apple" : {
        "weight" : 500
      },
      "battery" : {
        "aaa" : 10
      },
      "orange" : {
        "weight" : 300
      }
    },
    "list03" : {
      "Milk" : {
        "bottle" : 2
      },
      "battery" : {
        "aaa" : 10
      },
      "fish" : {
        "weight" : 500
      },
      "orange" : {
        "weight" : 300
      }
    }
  }

export {mokeTodoList}  